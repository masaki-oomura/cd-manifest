# Starter Kit : 運用編3日目 CD ハンズオン

**今後は https://gitlab.com/openshift-starter-kit/cd-practice を利用するため、このリポジトリはメンテンナンスされません。**
**CICD ハンズオンでは Git リポジトリに OCP クラスタ上にデプロイする Gitea を利用しますが、GitLab をベースとした手順の確認などに参考にして下さい。**

Container Stater Kit で実施するハンズオンの手順を紹介します。  
当ハンズオンによって、OpenShift GitOps を使ったアプリケーションの CD(継続的デリバリー) を体験することができます。

0 章は、当ハンズオンを実施するためにクラスターをセットアップする手順です。OpenShift クラスタの管理者権限が必要な手順が含まれていますので、ハンズオンまでにクラスタ管理者の方が行って下さい。

ハンズオンを受講する方は、1 章から実施して下さい。

---
## 【クラスタ管理者向け】
## [0 章: ハンズオンのためのクラスターセットアップ](./handson/text/Chapter-0.md)

---
## 【ハンズオン受講者向け】
## [1 章: CD でデプロイするアプリケーション環境の事前準備](./handson/text/Chapter-1.md)
## [2 章: OpenShift GitOps を使ったアプリケーションのデプロイ](./handson/text/Chapter-2.md)
## [3 章: 開発環境のアプリケーション更新をステージング環境～本番環境に反映](./handson/text/Chapter-3.md)
