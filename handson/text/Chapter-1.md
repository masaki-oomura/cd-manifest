# 1 章: CD でデプロイするアプリケーション環境の事前準備

Container Stater Kit で実施するハンズオンの手順を紹介します。  
当ハンズオンによって、OpenShift GitOps を使ったアプリケーションの CD(継続的デリバリー) を体験することができます。

本章では、アプリケーションの実行環境の事前準備に取り掛かります。  

当ハンズオンでは、アプリケーション本体と、そのアプリケーションを展開するためのマニフェストの2種類のコードを使用します。  
これらのコードは、GitLab.com にひな形となるリポジトリが用意されています。あなたはこのリポジトリからアプリケーションとマニフェストのコードを入手します。

---
## 1-1. アプリケーションとマニフェストのリポジトリのFork
初めに GitLab.com にサインインし、ひな形のリポジトリを Fork します。  
ひな形のリポジトリの URL は、それぞれ次の通りです。

* アプリケーションリポジトリ  
https://gitlab.com/openshift-starter-kit/cd-app

* マニフェストリポジトリ  
https://gitlab.com/openshift-starter-kit/cd-manifest

ひな形のリポジトリの画面で右側にある **"Fork"** ボタンをクリックします。

![Gitlab Fork 01](../img/gitlab-fork-01.PNG)

すると下図のような画面になります。

ここで、次の2点を設定します。その他の項目はそのままで構いません。

* "Project URL" の "Select a namespace" の部分をクリックし、あなた自身の Full Name を選択
* "Visibility level" で、"Private" を選択

![Gitlab Fork 02](../img/gitlab-fork-02.PNG)

最後に下部の **"Fork project"** ボタンをクリックすると、あなたのアカウントに Fork されます。

![Gitlab Fork 03](../img/gitlab-fork-03.PNG)

この操作を、アプリケーションリポジトリとマニフェストリポジトリの両方で行います。

---
## 1-2. GitLab のユーザ情報を環境変数に登録
次に、OpenShift クラスタに対し oc コマンドが実行可能な環境で、GitLab のユーザ情報を環境変数として登録します。
これら環境変数は、当ハンズオンのコマンド中や、ファイル中の文字列との置き換えなどで使用されるため、必要な手順です。

まずはあなたの GitLab アカウントの username とメールアドレスを登録します。  
**username と Full name を間違えないように注意してください。**  
GitLab の画面右上のユーザーアバター (丸形) のアイコンをクリックするとユーザーの名前が表示されますが、@(アットマーク)以下の方が username です。
![Gitlab Username 01](../img/gitlab-username-01.PNG)

```
## GitLabアカウントのusername
export GITLAB_USER=(ご自身のusername)

## GitLabアカウントので登録したメールアドレス
export GITLAB_USER_EMAIL=(ご自身のemail address)
```

次に、GitLab のアクセストークンを登録しますが、アクセストークンを作成していない人はここで取得しましょう。  
アクセストークンは、当ハンズオンで OpenShift Pipelines や OpenShift GitOps が、あなたの GitLab リポジトリを操作するために必要となります。

GitLab の画面右上のユーザーアバター (丸形) のアイコンをクリックし、**"Edit profile"** のメニューをクリックします。

![Gitlab Token 01](../img/gitlab-token-01.PNG)

次の画面の左にあるメニューから、**"Access Tokens"** をクリックし、"Personal Access Tokens" の画面を表示します。

![Gitlab Token 02](../img/gitlab-token-02.PNG)

開いた画面で次の項目を入力して、**"Create personal access token"** ボタンをクリックします。

* **Token name** : 好きな名前を入力
* **Expiration date** : アクセストークンの有効期限を自由に設定。設定しなくてもOKで、その場合はずっと有効になる。
* **Select scopes** : **"api"** にチェック

自動的にアクセストークンが作成され、上部に表示されます。  
**表示されているアクセストークンは、このページから移動すると二度と表示できなくなるため、どこかに記録しておきましょう。**  
※もしアクセストークンを失ってしまったら、もう一度同じ手順で作り直して下さい。

![Gitlab Token 03](../img/gitlab-token-03.PNG)

作成したアクセストークンを環境変数に登録します。
```
## GitLabアカウントのtoken
export GITLAB_TOKEN=(作成したtoken)
```

---
## 1-3. OpenShift クラスタへのログイン

当ハンズオンでは、あなたがクローンして入手したアプリケーションを、これまたあなたがクローンして入手したマニフェストを使って、OpenShift クラスタで稼働させます。  
OpenShift クラスタであなた用の Project を作成してアプリケーションを稼働させますが、Project を作成するために、OpenShift クラスタへログインします。

ログイン方法は、OpenShift クラスタで構成されている IdP(Identity Provider) によって多少異なります。このハンズオンテキストでは GitLab と連携する IdP を使ってログインする方法を採っていますが、他の IdP を構成している場合はその方法でログインしてください。

### 1-3-1. Web コンソールへのログイン (GitLab IdP の場合)

はじめに Web コンソールにログインします。  
ブラウザで Web コンソールの URL にアクセスし、一般ユーザの IdP を選択して下さい。下の図では、"gitlab-1" が一般ユーザ用の IdP です。

![Console login with gitlab](../img/user-login-01.PNG)

GitLab IdP の場合はここで GitLab のサインインの画面に移るため、サインインします。すでに GitLab にサインインしている場合は、この画面はスキップされるので何もしなくて構いません。

![Console login with gitlab](../img/user-login-02.PNG)

自動的に下図のような User Settings の画面が表示されます。  
ここでは OpenShift クラスタがあなたをユーザとして認証をするために、あなたの GitLab アカウントへのアクセスをリクエストしている旨の画面が表示されています。

 **"Authorize"** ボタンを押して OpenShift クラスタを承認します。

![Console login with gitlab](../img/user-login-03.PNG)

自動的に Web コンソールの画面に戻ります。画面右上に GitLab の名前 (Full Name) が表示されていることから、GitLab のアカウントを使ってログインできていることがわかります。

![Console login with gitlab](../img/user-login-04.PNG)

標準では一般ユーザーが Web コンソールにログインすると、Developer Perspective の画面が表示されます。また、初めてログインする場合は、画面中央に上図のようなポップアップが表示されます。  
**"Get Started"** から簡単なガイドを見ても構いませんし、**"Skip tour"** でスキップしても構いません。

何はともあれ、Web コンソールにログインすることができました。

### 1-3-2. CLI でのログイン

次に oc コマンドを使って CLI でログインします。  
先に Web コンソールにログインしておくと、CLI でのログインは非常に簡単に行うことができます。

Web コンソールの画面右上のユーザ名をクリックすると、**"Copy login command"** というメニューが表示されます。これをクリックします。

![CLI login](../img/user-login-05.PNG)

すると別のウィンドウで、Web コンソールにログインする際の IdP を選択する画面に移ります。ここで再び一般ユーザの IdP を選択します。

自動的にユーザ認証が行われ、左上に "Display Token" というリンクだけが表示された画面に移ります。このリンクをクリックします。

![CLI login](../img/user-login-06.PNG)

すると、API token が表示される画面に移ります。この画面の **"Log in with this token"** の欄に、`oc login` で始まるコマンドが表示されているので、このコマンドを全てコピーします。

![CLI login](../img/user-login-07.PNG)

OpenShift クラスタに対し oc コマンドが実行可能なターミナルで、コピーしたコマンドを実行すると、以下の実行例のように CLI でログインできます。  
試しに `oc whoami` と実行すると、あなたの GitLab のユーザ名 (=GITLAB_USER) が返ってきます。

```
oc login --token=sha256~xxxxxxxxxxxxx --server=https://api.xxxxxxxxxxxxx:6443

  [出力例]
  Logged into "https://api.xxxxxxxxxxxxx:6443" as "(GitLabのユーザ名)" using the token provided.

  You don't have any projects. You can try to create a new project, by running

      oc new-project <projectname>

  Welcome! See 'oc help' to get started.
```
```
oc whoami
```
---
## 1-4. Project の作成と CI パイプラインの作成と実行

それでは、OpenShift クラスタであなた用の Project を作成します。作成する Project は3つで、次のように名前を付けます。

* {GITLAB_USER}-develop
* {GITLAB_USER}-staging
* {GITLAB_USER}-production

名前から感づかれる方もいると思いますが、それぞれの Project は、開発環境、ステージング環境、本番環境の3つの環境を模しています。

また、アプリケーションのコンテナイメージをビルドする、CI パイプラインを作成し、実行します。  
当ハンズオンでは後段で、cd-app リポジトリのソースコードを更新します。このパイプラインは、更新されたアプリケーションコードから新しいコンテナイメージを簡単にビルドするために使います。

次のコマンドを全てコピーしてターミナルにペーストして実行すると、自動的に Project が作成され、パイプラインが作成/実行されます。

```
# Create your projects
oc new-project ${GITLAB_USER}-develop
oc new-project ${GITLAB_USER}-staging
oc new-project ${GITLAB_USER}-production

# Add image-puller role on staging/production project
oc policy add-role-to-user system:image-puller system:serviceaccount:${GITLAB_USER}-staging:default --namespace=${GITLAB_USER}-develop
oc policy add-role-to-user system:image-puller system:serviceaccount:${GITLAB_USER}-production:default --namespace=${GITLAB_USER}-develop

# Clone repositories
git clone https://${GITLAB_USER}:${GITLAB_TOKEN}@gitlab.com/${GITLAB_USER}/cd-manifest.git
git clone https://${GITLAB_USER}:${GITLAB_TOKEN}@gitlab.com/${GITLAB_USER}/cd-app.git

# Replace <GITLAB_~> string to your own variables
cd ./cd-manifest/
find . -type f -name "*.yaml" -print0 | xargs -0 sed -i "s/<GITLAB_USER>/${GITLAB_USER}/g"
find . -type f -name "*.yaml" -print0 | xargs -0 sed -i "s/<GITLAB_USER_EMAIL>/${GITLAB_USER_EMAIL}/g"
find . -type f -name "*.yaml" -print0 | xargs -0 sed -i "s/<GITLAB_TOKEN>/${GITLAB_TOKEN}/g"

# Push to remote cd-manifest repository
git config --global user.name ${GITLAB_USER}
git config --global user.email ${GITLAB_USER_EMAIL}
git commit -a -m "Replaced GITLAB_ info"
git push

# Create CI pipeline
oc project ${GITLAB_USER}-develop
oc create -f ./handson/pipelines/prep/gitlab-auth.yaml
oc secret link pipeline gitlab-token
oc create -f ./handson/pipelines/prep/tekton-pvc.yaml
oc create -f ./handson/pipelines/tasks/
oc create -f ./handson/pipelines/handson-pipeline.yaml

# Run CI pipeline
oc create -f ./handson/pipelines/handson-pipelinerun.yaml
```

---
## 1-5. パイプライン実行の確認
OpenShift Web コンソールに入って、パイプラインがうまく実行されたか確認してみましょう。

Web コンソールの左のメニューから **"Pipelines" > "Pipelines"** を選択し、画面上部の Project を選択する場所で **"{GITLAB_USER}-develop"** を選択します。  
そして、右側の画面で **"PipelineRuns"** のタブをクリックすると、"PLR" というマークが付いたエントリーが見つかります。  
これの Status が **"Succeeded"** になっていれば成功です。何か処理が動いているようであれば、終わるまで待ちます。(おおむね5分も待てばよいでしょう)

![PipelineRun Success 01](../img/pipelinerun-success-01.PNG)

エントリーの名前をクリックすると、実行されたパイプラインの中身やログを見ることができます。

![PipelineRun Success 02](../img/pipelinerun-success-02.PNG)

---
事前準備作業は以上です。